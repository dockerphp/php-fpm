# PHP 8.0 / FPM image

Ubuntu 20.04 PHP 8.0 FPM container image for Laravel projects. Packages are provided by [Ondřej Surý](https://deb.sury.org/).
